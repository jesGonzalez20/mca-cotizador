<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'correo' => Str::random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'rol_id'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('users')->insert([
            'correo' => Str::random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'rol_id'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
    }
}
