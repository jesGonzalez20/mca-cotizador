<?php

use Illuminate\Database\Seeder;

class RolTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nombre' => 'admin',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
    }
}
