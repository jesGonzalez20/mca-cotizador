<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Hash;
class UserModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['correo','password','rol_id'];

    public function add($user){
        return UserModel::create($user);
    }
    public function edit($user){
        return $this->fill($user)->save();
    }
    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    } 
}
