<?php

namespace App\Http\Controllers;

use App\models\UserModel;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
class UserController extends Controller{
    
    public function index(){
        return response()->json(UserModel::where('estatus',true)->orderBy('created_at','desc')->get(),200);
    }

    public function store(Request $request){
        if (!is_array($request->all())) {
            return ['error' => 'request must be an array'];
        }
        $rules = [
            'correo'=>'required|email',
            'password'=>'required',
            'rol_id'=>'required'
        ];
        try{
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            $user = new UserModel();
            $user->add($request->all());
            return ['created' => true];
        }catch(\Exception $e){
            return \Response::json(['created' => false,'msg'=>$e->getMessage()], 500);
        }
    }
    public function update(UserRequest $request, $idUser){

    }

}