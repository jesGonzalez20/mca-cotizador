import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-quoteadd',
    templateUrl: './quoteadd.component.html',
    styleUrls: ['./quoteadd.component.scss']
})
export class QuoteaddComponent implements OnInit {
    public costo: number = 2982273.72;
    public enganche: number = 0;
    public anualidad: number = 0;
    public saldo: number = 0;
    public mensualidad: number = 0;
    public pagosiva: any = [];
    public anios: number = 7;
    constructor() { }

    ngOnInit() {
    }

    calculate() {
        // mostrar en consola
        console.log(this.anualidad)
        console.log(this.anualidad)
        this.saldo = this.costo - this.enganche;
        this.mensualidad = (this.saldo - (this.anualidad * 8)) * 0.00941854419956791;
        console.log(this.mensualidad)
        this.pagosiva[0] = this.mensualidad 
        this.pagosiva[1] = (this.pagosiva[0]*1.06);
        this.pagosiva[2] = (this.pagosiva[1]*1.06);
        this.pagosiva[3] = (this.pagosiva[2]*1.06);
        this.pagosiva[4] = (this.pagosiva[3]*1.06);
        this.pagosiva[5] = (this.pagosiva[4]*1.06);
        this.pagosiva[6] = (this.pagosiva[5]*1.06);
        this.pagosiva[7] = (this.pagosiva[6]*1.06);
        console.log(this.pagosiva)
    
        
    }
   
}
