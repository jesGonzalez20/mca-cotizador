import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LotelistComponent } from './lotelist.component';

describe('LotelistComponent', () => {
  let component: LotelistComponent;
  let fixture: ComponentFixture<LotelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LotelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LotelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
