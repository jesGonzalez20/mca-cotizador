import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoteaddComponent } from './loteadd.component';

describe('LoteaddComponent', () => {
  let component: LoteaddComponent;
  let fixture: ComponentFixture<LoteaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoteaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoteaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
