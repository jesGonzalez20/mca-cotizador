import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public users:any = [];
  constructor(private userS:UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  private getUsers(){
    this.userS.getUsersActives().subscribe((response:any)=>{
      this.users  = response;
      console.log(response);
    },(error:any)=>{
      console.log(error.message);
    })
  }

}
