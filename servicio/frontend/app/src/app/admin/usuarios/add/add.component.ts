import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
    public user:any = {};
    constructor(private userS:UsersService, private router:Router) { }

    ngOnInit() {

    }

    onAdd(){
        this.user.rol_id = 1;
        this.userS.add(this.user).subscribe((res:any)=>{
            if(res.created)
                this.router.navigate(['/admin/users/list']);
        },(error:any)=>{    
            console.log(error.message);
        });
    }

}
