import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(private http: HttpClient) { }

    public getUsersActives(){
        return this.http.get('http://localhost:8000/api/v1/users');
    }
    public add(user:any){
        return this.http.post('http://localhost:3000/api/v1/users',{
            correo:user.email,
            password:user.password,
            rol_id:user.rol_id
        });
    }
}
