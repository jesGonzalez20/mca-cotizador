import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//INFORMATIVES PAGES
import { HomeComponent } from './pages/home/home.component';
import { AuthComponent } from './pages/auth/auth.component';

//ADMIN COMPONENTS
import { ListComponent } from './admin/usuarios/list/list.component';
import { AddComponent } from './admin/usuarios/add/add.component';
import { LotelistComponent } from './admin/lotes/lotelist/lotelist.component';
import { LoteaddComponent } from './admin/lotes/loteadd/loteadd.component';
import { CustomerlistComponent } from './customers/customerlist/customerlist.component';
import { CustomeraddComponent } from './customers/customeradd/customeradd.component';
import { RolslistComponent } from './rols/rolslist/rolslist.component';
import { RolsaddComponent } from './rols/rolsadd/rolsadd.component';
import { QuotelistComponent } from './quotes/quotelist/quotelist.component';
import { QuoteaddComponent } from './quotes/quoteadd/quoteadd.component';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AuthComponent,
        ListComponent,
        AddComponent,
        LotelistComponent,
        LoteaddComponent,
        CustomerlistComponent,
        CustomeraddComponent,
        RolslistComponent,
        RolsaddComponent,
        QuotelistComponent,
        QuoteaddComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
