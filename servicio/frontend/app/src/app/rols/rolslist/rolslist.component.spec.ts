import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolslistComponent } from './rolslist.component';

describe('RolslistComponent', () => {
  let component: RolslistComponent;
  let fixture: ComponentFixture<RolslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
