import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolsaddComponent } from './rolsadd.component';

describe('RolsaddComponent', () => {
  let component: RolsaddComponent;
  let fixture: ComponentFixture<RolsaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolsaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolsaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
