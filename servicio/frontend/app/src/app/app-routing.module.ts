import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { AuthComponent } from './pages/auth/auth.component';

//admin usuarios
import { ListComponent } from './admin/usuarios/list/list.component';
import { AddComponent } from './admin/usuarios/add/add.component';

//admin lotes
import { LotelistComponent } from './admin/lotes/lotelist/lotelist.component';
import { LoteaddComponent } from './admin/lotes/loteadd/loteadd.component';

import { CustomerlistComponent } from './customers/customerlist/customerlist.component';
import { CustomeraddComponent } from './customers/customeradd/customeradd.component';

import { RolslistComponent } from './rols/rolslist/rolslist.component';
import { RolsaddComponent } from './rols/rolsadd/rolsadd.component';

import { QuotelistComponent } from './quotes/quotelist/quotelist.component';
import { QuoteaddComponent } from './quotes/quoteadd/quoteadd.component';

const routes: Routes = [
    {
        path:'',
        component:HomeComponent
    },
    {
        path:'auth/login',
        component:AuthComponent
    },
    {
        path:'admin/users/list',
        component:ListComponent
    },
    {
        path:'admin/users/add',
        component:AddComponent
    },
    {
        path:'admin/lotes/list',
        component:LotelistComponent
    },
    {
        path:'admin/lotes/add',
        component:LoteaddComponent
    },
    {
        path:'admin/customers/list',
        component:CustomerlistComponent
    },
    {
        path:'admin/customers/add',
        component:CustomeraddComponent
    },
    {
        path:'admin/rols/list',
        component:RolslistComponent
    },
    {
        path:'admin/rols/add',
        component:RolsaddComponent
    },
    {
        path:'quots/list',
        component:QuotelistComponent
    },
    {
        path:'quots/add',
        component:QuoteaddComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
